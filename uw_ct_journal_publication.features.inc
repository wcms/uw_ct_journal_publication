<?php

/**
 * @file
 * uw_ct_journal_publication.features.inc
 */

/**
 * Implements hook_ctools_plugin_api().
 */
function uw_ct_journal_publication_ctools_plugin_api() {
  list($module, $api) = func_get_args();
  if ($module == "context" && $api == "context") {
    return array("version" => "3");
  }
  list($module, $api) = func_get_args();
  if ($module == "field_group" && $api == "field_group") {
    return array("version" => "1");
  }
  list($module, $api) = func_get_args();
  if ($module == "strongarm" && $api == "strongarm") {
    return array("version" => "1");
  }
}

/**
 * Implements hook_views_api().
 */
function uw_ct_journal_publication_views_api() {
  return array("api" => "3.0");
}

/**
 * Implements hook_strongarm_alter().
 */
function uw_ct_journal_publication_strongarm_alter(&$data) {
  if (isset($data['service_links_node_types'])) {
    $data['service_links_node_types']->value['uw_grebel_journal'] = 'uw_grebel_journal'; /* WAS: '' */
    $data['service_links_node_types']->value['uw_grebel_journal_publication'] = 'uw_grebel_journal_publication'; /* WAS: '' */
  }
}

/**
 * Implements hook_node_info().
 */
function uw_ct_journal_publication_node_info() {
  $items = array(
    'uw_grebel_journal' => array(
      'name' => t('Grebel Journal'),
      'base' => 'node_content',
      'description' => t('Grebel Journal'),
      'has_title' => '1',
      'title_label' => t('Journal Title'),
      'help' => '',
    ),
    'uw_grebel_journal_publication' => array(
      'name' => t('Grebel Journal Publication'),
      'base' => 'node_content',
      'description' => t('A publication of Grebel Journal'),
      'has_title' => '1',
      'title_label' => t('Title'),
      'help' => '',
    ),
  );
  return $items;
}
