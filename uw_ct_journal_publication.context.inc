<?php

/**
 * @file
 * uw_ct_journal_publication.context.inc
 */

/**
 * Implements hook_context_default_contexts().
 */
function uw_ct_journal_publication_context_default_contexts() {
  $export = array();

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'current_issue_cover';
  $context->description = 'Displays current issue cover sidebar.';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'publications/conrad-grebel-review/current-issue' => 'publications/conrad-grebel-review/current-issue',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-6bd529df27949b515b80cc4ecc4d46d0' => array(
          'module' => 'views',
          'delta' => '6bd529df27949b515b80cc4ecc4d46d0',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays current issue cover sidebar.');
  $export['current_issue_cover'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'grebel_issues_by_year_block';
  $context->description = 'Displays issues by year sidebar.';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'publications/conrad-grebel-review/current-issue' => 'publications/conrad-grebel-review/current-issue',
        'publications/conrad-grebel-review/past-issues/issues/*' => 'publications/conrad-grebel-review/past-issues/issues/*',
        'publications/conrad-grebel-review/past-issues/archive' => 'publications/conrad-grebel-review/past-issues/archive',
        'publications/conrad-grebel-review/past-issues/archive/*' => 'publications/conrad-grebel-review/past-issues/archive/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-2d5587850716fd6cae47b8cdb6e7410d' => array(
          'module' => 'views',
          'delta' => '2d5587850716fd6cae47b8cdb6e7410d',
          'region' => 'sidebar_second',
          'weight' => '-9',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays issues by year sidebar.');
  $export['grebel_issues_by_year_block'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'grebel_issues_search';
  $context->description = 'Display issues search box';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'publications/conrad-grebel-review/search-past-issues' => 'publications/conrad-grebel-review/search-past-issues',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-17bb67589ccece6732932baa62ac22a5' => array(
          'module' => 'views',
          'delta' => '17bb67589ccece6732932baa62ac22a5',
          'region' => 'content',
          'weight' => '9',
        ),
        'service_links-service_links' => array(
          'module' => 'service_links',
          'delta' => 'service_links',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Display issues search box');
  $export['grebel_issues_search'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'grebel_issues_search_sidebar';
  $context->description = '';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'publications/conrad-grebel-review/past-issues/search' => 'publications/conrad-grebel-review/past-issues/search',
        'publications/conrad-grebel-review/past-issues/search?*' => 'publications/conrad-grebel-review/past-issues/search?*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-17bb67589ccece6732932baa62ac22a5' => array(
          'module' => 'views',
          'delta' => '17bb67589ccece6732932baa62ac22a5',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  $export['grebel_issues_search_sidebar'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'grebel_issues_service_links';
  $context->description = 'Display service links icons under past issues view';
  $context->tag = 'Content';
  $context->conditions = array(
    'path' => array(
      'values' => array(
        'publications/conrad-grebel-review/past-issues/issues/*' => 'publications/conrad-grebel-review/past-issues/issues/*',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'service_links-service_links_not_node' => array(
          'module' => 'service_links',
          'delta' => 'service_links_not_node',
          'region' => 'content',
          'weight' => '10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Display service links icons under past issues view');
  $export['grebel_issues_service_links'] = $context;

  $context = new stdClass();
  $context->disabled = FALSE; /* Edit this to true to make a default context disabled initially */
  $context->api_version = 3;
  $context->name = 'past_issue_cover';
  $context->description = 'Displays past issue cover sidebar';
  $context->tag = 'Content';
  $context->conditions = array(
    'views' => array(
      'values' => array(
        'grebel_past_issues_by_year_volume_issue' => 'grebel_past_issues_by_year_volume_issue',
      ),
    ),
  );
  $context->reactions = array(
    'block' => array(
      'blocks' => array(
        'views-f1c0e1d65e6298bc891da4e9a6b93a0e' => array(
          'module' => 'views',
          'delta' => 'f1c0e1d65e6298bc891da4e9a6b93a0e',
          'region' => 'sidebar_second',
          'weight' => '-10',
        ),
      ),
    ),
  );
  $context->condition_mode = 0;

  // Translatables
  // Included for use with string extractors like potx.
  t('Content');
  t('Displays past issue cover sidebar');
  $export['past_issue_cover'] = $context;

  return $export;
}
