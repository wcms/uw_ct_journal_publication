<?php

/**
 * @file
 * uw_ct_journal_publication.features.user_permission.inc
 */

/**
 * Implements hook_user_default_permissions().
 */
function uw_ct_journal_publication_user_default_permissions() {
  $permissions = array();

  // Exported permission: 'create field_journal_status'.
  $permissions['create field_journal_status'] = array(
    'name' => 'create field_journal_status',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'create uw_grebel_journal content'.
  $permissions['create uw_grebel_journal content'] = array(
    'name' => 'create uw_grebel_journal content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'create uw_grebel_journal_publication content'.
  $permissions['create uw_grebel_journal_publication content'] = array(
    'name' => 'create uw_grebel_journal_publication content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_grebel_journal content'.
  $permissions['delete any uw_grebel_journal content'] = array(
    'name' => 'delete any uw_grebel_journal content',
    'roles' => array(
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete any uw_grebel_journal_publication content'.
  $permissions['delete any uw_grebel_journal_publication content'] = array(
    'name' => 'delete any uw_grebel_journal_publication content',
    'roles' => array(
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_grebel_journal content'.
  $permissions['delete own uw_grebel_journal content'] = array(
    'name' => 'delete own uw_grebel_journal content',
    'roles' => array(
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'delete own uw_grebel_journal_publication content'.
  $permissions['delete own uw_grebel_journal_publication content'] = array(
    'name' => 'delete own uw_grebel_journal_publication content',
    'roles' => array(
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_grebel_journal content'.
  $permissions['edit any uw_grebel_journal content'] = array(
    'name' => 'edit any uw_grebel_journal content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit any uw_grebel_journal_publication content'.
  $permissions['edit any uw_grebel_journal_publication content'] = array(
    'name' => 'edit any uw_grebel_journal_publication content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit field_journal_status'.
  $permissions['edit field_journal_status'] = array(
    'name' => 'edit field_journal_status',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own field_journal_status'.
  $permissions['edit own field_journal_status'] = array(
    'name' => 'edit own field_journal_status',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'edit own uw_grebel_journal content'.
  $permissions['edit own uw_grebel_journal content'] = array(
    'name' => 'edit own uw_grebel_journal content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'edit own uw_grebel_journal_publication content'.
  $permissions['edit own uw_grebel_journal_publication content'] = array(
    'name' => 'edit own uw_grebel_journal_publication content',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'node',
  );

  // Exported permission: 'enter uw_grebel_journal revision log entry'.
  $permissions['enter uw_grebel_journal revision log entry'] = array(
    'name' => 'enter uw_grebel_journal revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'enter uw_grebel_journal_publication revision log entry'.
  $permissions['enter uw_grebel_journal_publication revision log entry'] = array(
    'name' => 'enter uw_grebel_journal_publication revision log entry',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_grebel_journal authored by option'.
  $permissions['override uw_grebel_journal authored by option'] = array(
    'name' => 'override uw_grebel_journal authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_grebel_journal authored on option'.
  $permissions['override uw_grebel_journal authored on option'] = array(
    'name' => 'override uw_grebel_journal authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_grebel_journal promote to front page option'.
  $permissions['override uw_grebel_journal promote to front page option'] = array(
    'name' => 'override uw_grebel_journal promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_grebel_journal published option'.
  $permissions['override uw_grebel_journal published option'] = array(
    'name' => 'override uw_grebel_journal published option',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_grebel_journal revision option'.
  $permissions['override uw_grebel_journal revision option'] = array(
    'name' => 'override uw_grebel_journal revision option',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_grebel_journal sticky option'.
  $permissions['override uw_grebel_journal sticky option'] = array(
    'name' => 'override uw_grebel_journal sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_grebel_journal_publication authored by option'.
  $permissions['override uw_grebel_journal_publication authored by option'] = array(
    'name' => 'override uw_grebel_journal_publication authored by option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_grebel_journal_publication authored on option'.
  $permissions['override uw_grebel_journal_publication authored on option'] = array(
    'name' => 'override uw_grebel_journal_publication authored on option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_grebel_journal_publication promote to front page option'.
  $permissions['override uw_grebel_journal_publication promote to front page option'] = array(
    'name' => 'override uw_grebel_journal_publication promote to front page option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_grebel_journal_publication published option'.
  $permissions['override uw_grebel_journal_publication published option'] = array(
    'name' => 'override uw_grebel_journal_publication published option',
    'roles' => array(
      'administrator' => 'administrator',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_grebel_journal_publication revision option'.
  $permissions['override uw_grebel_journal_publication revision option'] = array(
    'name' => 'override uw_grebel_journal_publication revision option',
    'roles' => array(
      'administrator' => 'administrator',
      'content author' => 'content author',
      'content editor' => 'content editor',
      'site manager' => 'site manager',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'override uw_grebel_journal_publication sticky option'.
  $permissions['override uw_grebel_journal_publication sticky option'] = array(
    'name' => 'override uw_grebel_journal_publication sticky option',
    'roles' => array(
      'administrator' => 'administrator',
    ),
    'module' => 'override_node_options',
  );

  // Exported permission: 'view field_journal_status'.
  $permissions['view field_journal_status'] = array(
    'name' => 'view field_journal_status',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  // Exported permission: 'view own field_journal_status'.
  $permissions['view own field_journal_status'] = array(
    'name' => 'view own field_journal_status',
    'roles' => array(
      'administrator' => 'administrator',
      'site manager' => 'site manager',
    ),
    'module' => 'field_permissions',
  );

  return $permissions;
}
