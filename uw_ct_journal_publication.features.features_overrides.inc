<?php

/**
 * @file
 * uw_ct_journal_publication.features.features_overrides.inc
 */

/**
 * Implements hook_features_override_default_overrides().
 */
function uw_ct_journal_publication_features_override_default_overrides() {
  // This code is only used for UI in features. Exported alters hooks do the magic.
  $overrides = array();

  // Exported overrides for: variable.
  $overrides["variable.service_links_node_types.value|uw_grebel_journal"] = 'uw_grebel_journal';
  $overrides["variable.service_links_node_types.value|uw_grebel_journal_publication"] = 'uw_grebel_journal_publication';

  return $overrides;
}
