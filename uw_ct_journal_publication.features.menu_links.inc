<?php

/**
 * @file
 * uw_ct_journal_publication.features.menu_links.inc
 */

/**
 * Implements hook_menu_default_menu_links().
 */
function uw_ct_journal_publication_menu_default_menu_links() {
  $menu_links = array();

  // Exported menu link: main-menu:publications/conrad-grebel-review/current-issue.
  $menu_links['main-menu:publications/conrad-grebel-review/current-issue'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'publications/conrad-grebel-review/current-issue',
    'router_path' => 'publications/conrad-grebel-review/current-issue',
    'link_title' => 'Current issue',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 1,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -47,
    'parent_path' => 'node/284',
  );
  // Exported menu link: main-menu:publications/conrad-grebel-review/past-issues.
  $menu_links['main-menu:publications/conrad-grebel-review/past-issues'] = array(
    'menu_name' => 'main-menu',
    'link_path' => 'publications/conrad-grebel-review/past-issues',
    'router_path' => 'publications/conrad-grebel-review/past-issues',
    'link_title' => 'Past issues',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
      'alter' => TRUE,
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => -45,
    'parent_path' => 'node/284',
  );
  // Exported menu link: menu-site-manager-vocabularies:admin/structure/taxonomy/publication_issue.
  $menu_links['menu-site-manager-vocabularies:admin/structure/taxonomy/publication_issue'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/publication_issue',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Publication Issue',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
  );
  // Exported menu link: menu-site-manager-vocabularies:admin/structure/taxonomy/publication_term.
  $menu_links['menu-site-manager-vocabularies:admin/structure/taxonomy/publication_term'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/publication_term',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Publication Term',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
  );
  // Exported menu link: menu-site-manager-vocabularies:admin/structure/taxonomy/publication_type.
  $menu_links['menu-site-manager-vocabularies:admin/structure/taxonomy/publication_type'] = array(
    'menu_name' => 'menu-site-manager-vocabularies',
    'link_path' => 'admin/structure/taxonomy/publication_type',
    'router_path' => 'admin/structure/taxonomy/%',
    'link_title' => 'Publication Type',
    'options' => array(
      'attributes' => array(
        'title' => '',
      ),
    ),
    'module' => 'menu',
    'hidden' => 0,
    'external' => 0,
    'has_children' => 0,
    'expanded' => 0,
    'weight' => 0,
  );
  // Translatables
  // Included for use with string extractors like potx.
  t('Current issue');
  t('Past issues');
  t('Publication Issue');
  t('Publication Term');
  t('Publication Type');

  return $menu_links;
}
